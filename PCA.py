#librerias
import pandas as pd
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import numpy as np

#variables numericas
features=['Cantidad_Estud','Cod_Region','Cod_Depa','Cod_Muni','Cod_Nivel','Cod_Rama_Abstracta','Edad_RECO','Sexo_RECO','Cod_Area','Cod_Jornada','Cod_Ciclo','Etnia_RECO','Identificacion_Etnica_RECO','IE_Idioma_Materno_Recodificado','IE_Idioma_Materno_RECO','Idiom_Mater_Hab','Idiom_Mater_Ent','Idiom_Mater_Lee','Idiom_Mater_Esc','IE_Frecuencia_Uso_Idioma_Materno_RECO','IE_Segundo_Idioma_Materno_RECO','Entiendo_Oirlo','Entiendo_Leerlo','Puedo_Escribir','Lo_Hablo','Ed_Asistio_Preprimaria','Ed_Area_De_Escuela_Finalizo_Primaria','Ed_Repitio_Algun_Grado_Primaria','Gr_Primero','Gr_Segundo','Gr_Tercero','Gr_Cuarto','Gr_Quinto','Gr_Sexto','Ed_Trabaja_Actualmente','Ed_Jornada_Trabaja_RECO','Fm_Asistio_Escuela_Papa','Fm_Grado_Alcanzo_Papa_RECO','Fm_Asistio_Escuela_Mama','Fm_Grado_Alcanzo_Mama_RECO','Pp_Tierra','Pp_Madera_Rustica','Pp_Torta_Cemento','Pp_Piso_Granito','Pp_Piso_Ceramico','Pp_Madera_Fina','Pa_Madera_Rustica','Pa_Madera_Fina','Pa_Adobe','Pa_Lamina','Pa_Block','Pa_Ladrillo','Pt_Material_Perecedero','Pt_Lamina','Pt_Teja','Pt_Duralita','Pt_Terraza_Fundida','Al_Fuente_Natural','Al_Chorro_Publico','Al_Tuberia','Al_Comprada','Ab_Natural','Ab_Chorro','Ab_Comprada_Cisterna','Ab_Comprada_Embotellada','CC_Electricidad','CC_Ambiente_Cocina_Separada','CU_Leña','CU_Gas','CU_Electricidad','CC_Cuenta_Linea_Telefonica','CC_Tiene_Familia_Celular','CC_Servicio_Tv_Cable','CC_Servicio_Internet','Televisor','Refrigerador','Equipo_Sonido','VHS_DVD','Lavadora_Ropa','Secadora_Ropa','Horno_Microondas','Computadoras','Consola_Videojuegos','Otros_Electrodomesticos','Cc_No_Hay','Cc_Letrina','Cc_Inodoro','Cc_Familia_Automovil','Cc_A_Pie','Cc_Bicicleta','Cc_Motocicleta','Cc_Publico','Cc_Bus_Escolar','Cc_Automovil','Cc_0_15','Cc_16_30','Cc_31_45','Cc_46_60','Cc_61_75','Cc_76_90','Cc_91_105','Cc_M_105','Cc_Casa_Alquilada','Cc_Casa_Propia','Cc_Cuantos_Niveles_1','Cc_Cuantos_Niveles_2','Cc_Cuantos_Niveles_3','Cc_Cuantos_Niveles_4','Cc_Cuantos_Niveles_5','Cc_Cuantos_Niveles_Mas5','Cc_Cuantos_Dormitorios_1','Cc_Cuantos_Dormitorios_2','Cc_Cuantos_Dormitorios_3','Cc_Cuantos_Dormitorios_4','Cc_Cuantos_Dormitorios_5','Cc_Cuantos_Dormitorios_Mas5','Cc_Cuantas_Personas_1','Cc_Cuantas_Personas_2','Cc_Cuantas_Personas_3','Cc_Cuantas_Personas_4','Cc_Cuantas_Personas_5','Cc_Cuantas_Personas_Mas5','Cc_Recibe_Remesas','Tec_Poseen_Comp_Estable_Uso_Alumnos','Tec_Utiliza_Computadora_Realizar_Tareas_Estable','Tec_Periodos_Semana_Utiliza_Computadora_RECO','Tec_Horas_Diarias_Uso_Compu_Estab_RECO','Tec_Horas_Diarias_Uso_Compu_Casa_RECO','Tec_Utiliza_Internet_Para_Investigaciones','Tec_Recibido_Curso_Computacion','Mate_Periodos_Matematicas_Semana_RECO','Mate_Minutos_Dura_Periodo_Matematicas_RECO','Lect_Dias_Semana_Lee_Periodicos_RECO','Lc_Prensa_Libre','Lc_Siglo_XXI','Lc_El_Periodico','Lc_Nuestro_Diario','Lc_Diario_Centroamerica','Lc_Al_Dia','Lc_La_Hora','Lc_Publinews','Lc_Otro','Lect_Libros_Completos_Ha_Leido_RECO','Lect_Periodos_Lectura_Semana_RECO','Lect_Minutos_Dura_Periodo_Lectura_RECO']

mate=['Measure_Mate','Desempeño_Mate','Logro_Mate']
lect=['Measure_Lect','Desempeño_Lect','Logro_Lect']


#datos numericos
x = df.loc[:, features].fillna(0).values

#estandarizar datos
x = StandardScaler().fit_transform(x)

#parametros de pca
pca = PCA(n_components=n)
#pca
principalComponents = pca.fit_transform(x)
#componentes
pca.components_
#varianza porcentual
pca.explained_variance_ratio_

#dataframe con componentes
principalDf = pd.DataFrame(data = principalComponents, columns = ['pc1', 'pc2'])
#analisis de matematica
finalDf = pd.concat([principalDf, df['Logro_Mate']], axis = 1)

#variables mas influyentes
i=np.argwhere(pc1 > 0.1)

#modulo valores propios porcentuales
ev=pca.explained_variance_ratio_

plt.plot(np.cumsum(ev))

#grafica de target con componentes principales
fig = plt.figure(figsize = (8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Principal Component 1', fontsize = 15)

ax.set_ylabel('Principal Component 2', fontsize = 15)

ax.set_title('2 component PCA', fontsize = 20)

targets = [0,1]
colors = ['r', 'g']
for target, color in zip(targets,colors):
  indicesToKeep = finalDf['Logro_Mate'] == target
  ax.scatter(finalDf.loc[indicesToKeep, 'pc1'], finalDf.loc[indicesToKeep, 'pc2'], c = color, s = 50)

ax.legend(targets)
ax.grid()
plt.show()
